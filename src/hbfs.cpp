/*
	HybridFS : Hybrid filesystem based on FUSE
	Copyright (C) 2015 Jaewon Choi <jainersoer@ajou.ac.kr>

	This program cannot be distributed anyway.

	gcc -Wall `pkg-config fuse --cflags --libs` -o hbfs  hbfs.c -lfuse -lsqlite3
*/

#define FUSE_USE_VERSION 26

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#ifdef linux
/* For pread()/pwrite()/utimensat() */
#define _XOPEN_SOURCE 700
#endif // linux

#include <fuse.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>

#include <sys/time.h>
#ifdef HAVE_SETXATTR
#include <sys/xattr.h>
#endif // HAVE_SETXATTR

#include <sqlite3.h>

#include <set>
#include <map>

#include "params.h"

static hb_state* hb_data = NULL;
static FILE* fp = NULL;
static sqlite3 *db = NULL;
static int migration_frequency = 0;
static struct fuse_operations hb_oper;

pthread_mutex_t migdoings_mutex;
pthread_mutex_t migfails_mutex;
std::map<int, pthread_mutex_t> migdoings;
std::set<int> migfails;

// utiltiy
static void get_fullpath(char fullpath[BUFSIZ], const char *path)
{
	sprintf(fullpath, "%s/%s", hb_data->ssdpath, path);
}

static int convertToRegular(void * sender, int argc, char** argv, char** azColName)
{
	struct stat *stbuf = (struct stat*)sender;

	if (atoi(argv[EM_LOCATION]) == 1) {
		struct stat hddst;
		stat(argv[EM_HDD_PATH], &hddst);

		stbuf->st_mode = S_IFREG;
		stbuf->st_size = hddst.st_size;
		stbuf->st_blksize = hddst.st_blksize;
		stbuf->st_blocks = hddst.st_blocks;
		stbuf->st_atime = hddst.st_atime;
		stbuf->st_mtime = hddst.st_mtime;
		stbuf->st_ctime = hddst.st_ctime;

		stbuf->st_uid = hddst.st_uid;
		stbuf->st_gid = hddst.st_gid;
	}

	return 0;
}

static int hb_getattr(const char *path, struct stat *stbuf)
{
	int res;
	char fullpath[BUFSIZ];

	get_fullpath(fullpath, path);

	res = lstat(fullpath, stbuf);

	{
		int qResult;
		char * zErrMsg;
		char sql[BUFSIZ];
		sprintf(sql, "select * from migrants where ssd_path = '%s';", fullpath);

		qResult = sqlite3_exec(db, sql, convertToRegular, stbuf, &zErrMsg);
		if(qResult != SQLITE_OK){
			fprintf(stderr, "SQL error : %s\n", sqlite3_errmsg(db));
			sqlite3_free(zErrMsg);
		}
	}

	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int hb_access(const char *path, int mask)
{
	int res;
	char fullpath[BUFSIZ];

	sprintf(fullpath, "%s/%s", hb_data->ssdpath, path);

	res = access(fullpath, mask);
	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int hb_readlink(const char *path, char *buf, size_t size)
{
	int res;
	char fullpath[BUFSIZ];

	sprintf(fullpath, "%s/%s", hb_data->ssdpath, path);
	res = readlink(fullpath, buf, size - 1);
	if (res == -1) {
		return -errno;
	}

	buf[res] = '\0';

	return 0;
}

static int hb_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
			off_t offset, struct fuse_file_info *fi)
{
	DIR *dp;
	struct dirent *de;

	(void) offset;
	(void) fi;

	char fullpath[BUFSIZ];

	sprintf(fullpath, "%s/%s", hb_data->ssdpath, path);

	dp = opendir(fullpath);
	if (dp == NULL) {
		return -errno;
	}

	while ((de = readdir(dp)) != NULL) {
		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = de->d_ino;
		st.st_mode = de->d_type << 12;
		if (filler(buf, de->d_name, &st, 0)) {
			break;
		}
	}

	closedir(dp);

	return 0;
}

static int hb_mknod(const char *path, mode_t mode, dev_t rdev)
{
	int res;

	char fullpath[BUFSIZ];
	sprintf(fullpath, "%s/%s", hb_data->ssdpath, path);

	if (S_ISREG(mode)) {
		res = open(fullpath, O_CREAT | O_EXCL | O_WRONLY, mode);
		if (res >= 0) {
			res = close(res);
		}
	} else if (S_ISFIFO(mode)) {
		res = mkfifo(fullpath, mode);
	} else {
		res = mknod(fullpath, mode, rdev);
	}

	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int hb_mkdir(const char *path, mode_t mode)
{
	int res;

	char fullpath[BUFSIZ];

	sprintf(fullpath, "%s/%s", hb_data->ssdpath, path);

	res = mkdir(fullpath, mode);
	if (res == -1) {
		return -errno;
	}

	return 0;
}
static int getMetadata( void * NotUsed, int argc, char ** argv, char ** azColName){
	if (atoi(argv[EM_LOCATION]) == 1) { // location
		unlink(argv[EM_HDD_PATH]);
	}

	return 0;
}
static int hb_unlink(const char *path)
{
	int res;

	char fullpath[BUFSIZ];
	sprintf(fullpath, "%s/%s", hb_data->ssdpath, path);

	{
		int qResult;
		char * zErrMsg;
		char sql[BUFSIZ];
		sprintf(sql, "select * from migrants where ssd_path = '%s';", fullpath);

		qResult = sqlite3_exec(db, sql, getMetadata, 0, &zErrMsg);
		if(qResult != SQLITE_OK){
			// error check
			fprintf(stderr, "SQL error : %s\n", sqlite3_errmsg(db));
			sqlite3_free(zErrMsg);
		}else{

			sprintf(sql, "delete from migrants where ssd_path = '%s';", fullpath);
			qResult = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
			if(qResult != SQLITE_OK) {
				// error check
				fprintf(stderr, "SQL error : %s\n", sqlite3_errmsg(db));
				sqlite3_free(zErrMsg);
			}
		}
	}

	res = unlink(fullpath);
	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int hb_rmdir(const char *path)
{
	int res;

	char fullpath[BUFSIZ];
	sprintf(fullpath, "%s/%s", hb_data->ssdpath, path);

	res = rmdir(fullpath);
	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int hb_symlink(const char *from, const char *to)
{
	int res;

	char fullpath_from[BUFSIZ], fullpath_to[BUFSIZ];
	get_fullpath(fullpath_from, from);
	get_fullpath(fullpath_to, to);

	res = symlink(fullpath_from, fullpath_to);
	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int hb_rename(const char *from, const char *to)
{
	int res;

	char fullpath_from[BUFSIZ], fullpath_to[BUFSIZ];
	get_fullpath(fullpath_from, from);
	get_fullpath(fullpath_to, to);

	res = rename(fullpath_from, fullpath_to);
	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int hb_link(const char *from, const char *to)
{
	int res;

	char fullpath_from[BUFSIZ], fullpath_to[BUFSIZ];
	get_fullpath(fullpath_from, from);
	get_fullpath(fullpath_to, to);

	res = link(fullpath_from, fullpath_to);
	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int hb_chmod(const char *path, mode_t mode)
{
	int res;

	char fullpath[BUFSIZ];
	get_fullpath(fullpath, path);

	res = chmod(fullpath, mode);
	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int hb_chown(const char *path, uid_t uid, gid_t gid)
{
	int res;
	char fullpath[BUFSIZ];
	get_fullpath(fullpath, path);

	res = lchown(fullpath, uid, gid);
	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int hb_truncate(const char *path, off_t size)
{
	int res;
	char fullpath[BUFSIZ];
	get_fullpath(fullpath, path);

	res = truncate(fullpath, size);
	if (res == -1) {
		return -errno;
	}

	return 0;
}

#ifdef HAVE_UTIMENSAT
static int hb_utimens(const char *path, const struct timespec ts[2])
{
	int res;
	char fullpath[BUFSIZ];

	sprintf(fullpath, "%s/%s", hb_data->ssdpath, path);

	/* don't use utime/utimes since they follow symlinks */
	res = utimensat(0, fullpath, AT_SYMLINK_NOFOLLOW);
	if (res == -1) {
		return -errno;
	}

	return 0;
}
#endif // HAVE_UTIMENSAT

static int hb_open(const char *path, struct fuse_file_info *fi)
{
	int res;

	char fullpath[BUFSIZ];

	sprintf(fullpath, "%s/%s", hb_data->ssdpath, path);

	res = open(fullpath, fi->flags);
	if (res == -1) {
		return -errno;
	}

	close(res);

	return 0;
}

static int hb_read(const char *path, char *buf, size_t size, off_t offset,
			struct fuse_file_info *fi)
{
	int fd;
	int res;
	char fullpath[BUFSIZ];

	(void) fi;

	sprintf(fullpath, "%s/%s", hb_data->ssdpath, path);

	fd = open(fullpath, O_RDONLY);
	if (fd == -1) {
		return -errno;
	}

	res = pread(fd, buf, size, offset);
	if (res == -1) {
		res = -errno;
	}

	close(fd);
	return res;
}

static int hb_write(const char *path, const char *buf, size_t size,
					off_t offset, struct fuse_file_info *fi)
{
	int fd;
	int res;
	char fullpath[BUFSIZ];
	struct stat stbuf;
	static const unsigned int kThreshold = 1024*1024*1024;

	(void) fi;

	fprintf(fp, "%s\n", path);
	sprintf(fullpath, "%s/%s", hb_data->ssdpath, path);

	fd = open(fullpath, O_WRONLY);
	if (fd == -1) {
		return -errno;
	}
#ifndef MIGRATION_OFF

	res = lstat(fullpath, &stbuf);
	if (res == -1) {
		return -errno;
	}


	if (S_ISREG(stbuf.st_mode) && stbuf.st_size > kThreshold) {
		auto ino = stbuf.st_ino;

		pthread_mutex_lock(&migdoings_mutex);
		auto it = migdoings.find(ino);
		bool isMigrating = it != migdoings.end();
		pthread_mutex_unlock(&migdoings_mutex);

		if (isMigrating) {
			pthread_mutex_lock(&migfails_mutex);

			// XXX : Assume that migfails of this ino can not exist before this line.
			migfails.insert(ino);

			pthread_mutex_unlock(&migfails_mutex);
		} else {
			pthread_mutex_lock(&migdoings_mutex);

			auto element = std::pair<int, pthread_mutex_t>(ino, PTHREAD_MUTEX_INITIALIZER);

			migdoings.insert(element);

			pthread_mutex_unlock(&migdoings_mutex);
		}

		pthread_mutex_lock(&migdoings[ino]);

		int inputFd, outputFd, openFlags;
		mode_t filePerms;
		ssize_t numRead;
		char chunk[4096];
		char newPath[BUFSIZ];
		sprintf(newPath, "%s/%s", hb_data->hddpath, path);

		inputFd = open(fullpath, O_RDONLY);
		if (inputFd == -1) {
			perror("opening full path file");
			exit(1);
		}

		openFlags = O_CREAT | O_WRONLY | O_TRUNC;
		filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP |
					S_IROTH | S_IWOTH; // rw-rw-rw-
		outputFd = open(newPath, openFlags, filePerms);
		if (outputFd == -1) {
			perror("opening new file");
			exit(1);
		}

		while ((numRead = read(inputFd, chunk, 4096)) > 0) {
			// XXX : fix map to hash_set
			auto ft = migfails.find(ino);
			
			if (ft != migfails.end()) {
				remove(newPath);

				pthread_mutex_lock(&migfails_mutex);
				migfails.erase(ft);
				pthread_mutex_unlock(&migfails_mutex);

				res = pwrite(fd, buf, size, offset);
				if (res == -1) {
					res = -errno;
				}

				pthread_mutex_unlock(&migdoings[ino]);
				pthread_mutex_lock(&migdoings_mutex);
				migdoings.erase(ino);
				pthread_mutex_unlock(&migdoings_mutex);
				
				return res;
			}

			if (write(outputFd, chunk, numRead) != numRead) {
				perror("couldn't write while buffer");
			}
		}

		if (numRead == -1) {
			perror("read");
			exit(1);
		}
		unlink(fullpath);
		symlink(newPath, fullpath);

		{
			int rc;
			char * zErrMsg;
			struct stat info;
			unsigned long long int ino_num = 0;

			stat(newPath, &info);
			ino_num = (1llu << 32llu) + info.st_ino; // It will be stored in HDD.
			sprintf(chunk, "insert into migrants values (%llu,%d, '%s', '%s'); ", ino_num, 1, fullpath, newPath);

			rc = sqlite3_exec(db, chunk, NULL, 0, &zErrMsg);
			if (rc != SQLITE_OK) {
				fprintf(stderr, "SQL error: %s\n", zErrMsg);
				sqlite3_free(zErrMsg);
			}
		}

		++migration_frequency;

		close(fd);
		close(inputFd);

		fd = outputFd;

		// Migration completely finished.
		pthread_mutex_unlock(&migdoings[ino]);

		pthread_mutex_lock(&migdoings_mutex);
		migdoings.erase(ino);
		pthread_mutex_unlock(&migdoings_mutex);
	}
#endif
	res = pwrite(fd, buf, size, offset);
	if (res == -1) {
		res = -errno;
	}

	close(fd);
	return res;
}

static int hb_statfs(const char *path, struct statvfs *stbuf)
{
	int res;
	char fullpath[BUFSIZ];
	get_fullpath(fullpath, path);

	res = statvfs(fullpath, stbuf);
	if (res == -1) {
		return -errno;
	}

	return 0;
}

static int hb_release(const char *path, struct fuse_file_info *fi)
{
	/* Just a stub. This method is optional and can safely be left
	   unimplemented */

	(void) path;
	(void ) fi;

	return 0;
}

static int hb_fsync(const char *path, int isdatasync,
					struct fuse_file_info *fi)
{
	/* Just a stub. This method id optional and can safely be left
	   unimplemented */

	(void) path;
	(void) isdatasync;
	(void) fi;

	return 0;
}

#ifdef HAVE_POSIX_FALLOCATE
static int hb_fallocate(const char *path, int mode,
			off_t offset, off_t length, struct fuse_file_info *fi)
{
	int fd;
	int res;
	char fullpath[BUFSIZ];

	(void) fi;

	get_fullpath(fullpath, path);

	if (mode) {
		return -EOPNOTSUPP;
	}

	fd = open(fullpath, O_WRONLY);
	if (fd == -1) {
		return -errno;
	}

	res = -posix_fallocate(fd, offset, length);

	close(fd);

	return res;
}
#endif // HAVE_POSIX_FALLOCATE

static void *hb_init(struct fuse_conn_info *conn)
{
	int res = 0;

	fp = fopen("exec.log", "wt");
	if (fp == NULL) {
		return NULL;
	}

	res = sqlite3_open(hb_data->dbpath, &db);
	if (res) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		sqlite3_close(db);
		db = NULL;
		return NULL;
	}

	return NULL;
}

static void hb_destroy(void *userdata)
{
	if (hb_data) {
		free(hb_data);
		hb_data = NULL;
	}

	fclose(fp);

	sqlite3_close(db);

	printf("%d\n", migration_frequency);
}
/*
static struct fuse_operations hb_oper = {
	.getattr	= hb_getattr,
	.readdir	= hb_readdir,
	.open		= hb_open,
	.read		= hb_read,
	.write		= hb_write,
	.access		= hb_access,
	.readlink	= hb_readlink,
	.mkdir		= hb_mkdir,
	.mknod		= hb_mknod,
#ifdef HAVE_UTIMENSAT
	.utimens	= hb_utimens
#endif
	.unlink		= hb_unlink,
	.rmdir		= hb_rmdir,
	.symlink	= hb_symlink,
	.rename		= hb_rename,
	.link		= hb_link,
	.chmod		= hb_chmod,
	.chown		= hb_chown,
	.truncate	= hb_truncate,
	.statfs		= hb_statfs,
	.release	= hb_release,
	.fsync		= hb_fsync,
	.init		= hb_init,
	.destroy	= hb_destroy
};
*/
int main(int argc, char *argv[])
{
	umask(0);

	if (argc < 5) {
		fprintf(stderr, "Usage: ./hbfs db-path ssd-path hdd-path mountpoint\n");
		exit(1);
	}

	hb_data = (struct hb_state*)malloc(sizeof(struct hb_state));
	if (hb_data == NULL) {
		perror("main calloc");
		abort();
	}

	hb_data->dbpath = realpath(argv[argc-4], NULL);
	hb_data->ssdpath = realpath(argv[argc-3], NULL);
	hb_data->hddpath = realpath(argv[argc-2], NULL);
	argv[argc-4] = argv[argc-1];
	argv[argc-3] = NULL;
	argv[argc-2] = NULL;
	argv[argc-1] = NULL;
	argc--;
	argc--;
	argc--;
	hb_oper.getattr	= hb_getattr,
	hb_oper.readdir	= hb_readdir,
	hb_oper.open		= hb_open,
	hb_oper.read		= hb_read,
	hb_oper.write		= hb_write,
	hb_oper.access		= hb_access,
	hb_oper.readlink	= hb_readlink,
	hb_oper.mkdir		= hb_mkdir,
	hb_oper.mknod		= hb_mknod,
#ifdef HAVE_UTIMENSAT
	hb_oper.utimens	= hb_utimens
#endif
	hb_oper.unlink		= hb_unlink,
	hb_oper.rmdir		= hb_rmdir,
	hb_oper.symlink	= hb_symlink,
	hb_oper.rename		= hb_rename,
	hb_oper.link		= hb_link,
	hb_oper.chmod		= hb_chmod,
	hb_oper.chown		= hb_chown,
	hb_oper.truncate	= hb_truncate,
	hb_oper.statfs		= hb_statfs,
	hb_oper.release	= hb_release,
	hb_oper.fsync		= hb_fsync,
	hb_oper.init		= hb_init,
	hb_oper.open		= hb_open,
	hb_oper.read		= hb_read,
	hb_oper.write		= hb_write,
	hb_oper.access		= hb_access,
	hb_oper.readlink	= hb_readlink,
	hb_oper.mkdir		= hb_mkdir,
	hb_oper.mknod		= hb_mknod,
#ifdef HAVE_UTIMENSAT
	hb_oper.utimens	= hb_utimens
#endif
	hb_oper.unlink		= hb_unlink,
	hb_oper.rmdir		= hb_rmdir,
	hb_oper.symlink	= hb_symlink,
	hb_oper.rename		= hb_rename,
	hb_oper.link		= hb_link,
	hb_oper.chmod		= hb_chmod,
	hb_oper.chown		= hb_chown,
	hb_oper.truncate	= hb_truncate,
	hb_oper.statfs		= hb_statfs,
	hb_oper.release	= hb_release,
	hb_oper.fsync		= hb_fsync,
	hb_oper.init		= hb_init,
	hb_oper.destroy		= hb_destroy;

	return fuse_main(argc, argv, &hb_oper, hb_data);
}
