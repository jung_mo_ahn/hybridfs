#ifndef PARAMS_H_
#define PARAMS_H_

struct hb_state {
	char *ssdpath;
	char *hddpath;
	char *dbpath;
};

enum E_METADATA {
	EM_ID, EM_LOCATION, EM_SSD_PATH, EM_HDD_PATH, EM_COUNT
};

struct hb_metadata {
	unsigned long long int id;
	int location; // 0 : SSD, 1 : HDD
	char ssd_path[BUFSIZ];
	char hdd_path[BUFSIZ];
};

#endif // PARAMS_H_
